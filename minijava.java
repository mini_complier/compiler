import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;


public class minijava {
	public static void main(String[] args) throws Exception{
		ANTLRInputStream input =new  ANTLRInputStream(System.in);
		MiniJavaLexer lexer =new MiniJavaLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		MiniJavaParser parser =new MiniJavaParser(tokens);
		ParseTree tree = parser.translationUnit();
		System.out.println(tree.toStringTree(parser));
	}


}
